#!/usr/bin/env scriptisto

// scriptisto-begin
// script_src: src/main.rs
// build_cmd: cargo build --release --color always && strip ./target/release/script
// target_bin: ./target/release/script
// files:
//  - path: Cargo.toml
//    content: |
//     package = { name = "script", version = "0.1.0", edition = "2018"}
//     [dependencies]
// scriptisto-end

macro_rules! command {
    ($command:expr, $($arg0:expr $(, $arg:expr)*)?) => {{
        use std::process::{Command, Stdio};
        println!("Running `{}`, with args: [{}]", $command, concat!($("\"", $arg0, "\"" $(, ", ", "\"", $arg, "\"")*)?));
        let mut command = Command::new($command);
        $(
            command.arg($arg0);
            $(command.arg($arg);)*
        )?
        command.stdout(Stdio::piped());
        command.spawn().unwrap().wait()
    }};
}

fn main() {
    assert!(command!("cargo", "clippy").unwrap().success());
    assert!(command!("cargo", "doc", "--document-private-items")
        .unwrap()
        .success());
    assert!(command!("cargo", "test").unwrap().success());
}
