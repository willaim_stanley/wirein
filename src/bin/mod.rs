//! A wrapper around the crate's library

// *********
// | Lints |
// *********
#![warn(missing_docs)]
// Clippy //
#![warn(clippy::all)]
#![warn(clippy::cargo)]
#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
// This lint reports false positives from unknown reasons (probably because of macros creating underscore variables and using them).
#![allow(clippy::used_underscore_binding)]
// This lint is triggered by dependencies and can't be fixed by us.
#![allow(clippy::multiple_crate_versions)]

// ***********
// | Imports *
// ***********

fn main() {}
