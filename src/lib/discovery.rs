//! A module which performs the discovery of metadata files and decisions about searching subdirectories
use crate::metadata;
use snafu::{ResultExt, Snafu};
use std::borrow::Borrow;
use std::fs::DirEntry;
use std::path::Path;

/// A mode of inspection for a directory
///
/// This enum dictates how contents of a directory should (or should not) be inspected
pub enum DirectoryInspectionMode {
    /// Skip the directory altogether.
    ///
    /// This does not even require reading it's contents.
    Skip,
    /// Inspect the directory fully
    ///
    /// This means that all children of the directory should be inspected.
    Inspect,
}

/// A single entry returned from [`filter_directory_entries`]
pub enum FilteredEntry<'a> {
    /// A file entry
    File(&'a DirEntry),
    /// A directory entry with associated [`DirectoryInspectionMode`]
    Directory {
        /// The original entry
        entry: &'a DirEntry,
        /// The [`DirectoryInspectionMode`] for this entry
        inspection_mode: DirectoryInspectionMode,
    },
}

#[derive(Debug, Snafu)]
enum EntryFilteringError {
    #[snafu(display(
        "Attempted to process a pattern which is not valid glob. Offending pattern: {:?}",
        pattern
    ))]
    InvalidGlob {
        pattern: String,
        source: glob::PatternError,
    },
}

fn compile_patterns(patterns: &[String]) -> Vec<glob::Pattern> {
    patterns
        .iter()
        .filter_map(|pattern| {
            glob::Pattern::new(pattern)
                .context(InvalidGlob { pattern })
                .map_err(|err| {
                    warn!("Warning: Encountered pattern compilation error: {}", err);
                    err
                })
                .ok()
        })
        .collect()
}

fn test_match<'a>(
    path: &impl AsRef<Path>,
    patterns: &mut impl Iterator<Item = &'a glob::Pattern>,
) -> bool {
    patterns.any(|pattern| {
        pattern.borrow().matches_path_with(
            path.as_ref(),
            glob::MatchOptions {
                case_sensitive: true,
                require_literal_separator: true,
                require_literal_leading_dot: false,
            },
        )
    })
}

/// Filter the passed directory entries by current directory mode and directory metadata
///
/// # Examples
/// ```
/// # use wirein::{discovery::{DirectoryInspectionMode, filter_directory_entries}, metadata};
/// # use std::path::Path;
/// let paths = vec![
///     Path::new("some unmatched path"),
///     Path::new("abc"),
///     Path::new("a0x"),
///
///     Path::new("a01"),
///     Path::new("axc"),
/// ];
///
/// let result = filter_directory_entries(
///     &DirectoryInspectionMode::Inspect, // We want the directory to be fully inspected (i.e.: don't skip any children).
///     &metadata::Directory {
///         excludes: vec!["a*".to_string()],
///         includes: vec![
///             "abc".to_string(),
///             "*x".to_string(),
///         ],
///     },
///     paths.iter(),
/// );
/// assert_eq!(result, vec![
///     &Path::new("some unmatched path"),
///     &Path::new("abc"),
///     &Path::new("a0x"),
/// ]);
/// ```
#[must_use = "This function is purely deterministic, and therefore calling it without using the result is pointless."]
pub fn filter_directory_entries<P: AsRef<Path>>(
    mode: &DirectoryInspectionMode,
    metadata: &metadata::Directory,
    paths: impl Iterator<Item = P>,
) -> Vec<P> {
    match mode {
        DirectoryInspectionMode::Skip => vec![],
        DirectoryInspectionMode::Inspect => {
            let includes = compile_patterns(&metadata.includes);
            let excludes = compile_patterns(&metadata.excludes);

            paths
                .filter(|path| {
                    let is_excluded = test_match(path, &mut excludes.iter());
                    let is_included = test_match(path, &mut includes.iter());
                    (!is_excluded) || is_included
                })
                .collect()
        }
    }
}
