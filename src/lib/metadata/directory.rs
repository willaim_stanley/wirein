//! The directory metadata
//!
//! For further information, look at [`Directory`].

use serde::Deserialize;

/// A directory metadata
///
/// This metadata dictates how to inspect the directory children
#[derive(Debug, Clone, Deserialize)]
pub struct Directory {
    /// The excluded files from inspection
    ///
    /// This field has a lower priority than [`Directory::includes`].
    pub excludes: Vec<String>,
    /// The included files in inspection
    ///
    /// Since, by default, all files are included, this is only useful to re-include children excluded by the `excludes` field.
    ///
    /// This field always has a higher priority than the [`Directory::excludes`].
    /// This means that an include of `*` will make any excludes ineffective, even if they are more specific.
    /// Note that this behavior may change in the future.
    pub includes: Vec<String>,
}
