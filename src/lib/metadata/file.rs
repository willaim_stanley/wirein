//! A file metadata
//!
//! For further information, look at [`File`].
use serde::Deserialize;
use std::path::PathBuf;

/// Metadata for a single file
#[derive(Debug, Clone, Deserialize)]
pub struct File {
    /// The target places where simlinks should be placed to
    pub targets: Vec<PathBuf>,
}
