//! This crate amis to helps you manage files used by other programs
//!
//! This is accomplished by letting you move files to any location you like and then creating simple metadata files alongside them that tell this crate where to put simlinks leading to the original files

// *********
// | Lints |
// *********
#![forbid(unsafe_code)]
#![deny(missing_docs)]
// Hopefully we can enable this in the future, but it's out of scope during the initial development phase.
#![allow(missing_doc_code_examples)]
// Clippy //
#![warn(clippy::all)]
#![warn(clippy::cargo)]
#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
// This lint reports false positives from unknown reasons (probably because of macros creating underscore variables and using them).
#![allow(clippy::used_underscore_binding)]
// This lint is triggered by dependencies and can't be fixed by us.
#![allow(clippy::multiple_crate_versions)]

use core::fmt::Display;
use std::fmt::Formatter;
use std::path::Path;

// ***********
// | Modules |
// ***********
pub mod discovery;
pub mod metadata;

// **********
// | Macros |
// **********
#[macro_use]
extern crate log;

/// An error representing a path that is not a directory given to a function which expects directory
pub struct PathIsNotDirectory {}
impl Display for PathIsNotDirectory {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "The given path is not a directory")
    }
}

/// Process the given directory
///
/// # Errors
/// Returns error if the given path is not a directory
pub fn process<P: AsRef<Path>>(_path: P) -> Result<(), PathIsNotDirectory> {
    todo!()
}
